## Hi (~username~)!

You have been added as a user on (~HOSTNAME~)
As a registered user, you were affiliated as follows:
(~clanList~)


To identify yourself, use the following credentials:
- username: `(~username~)`
- password: `(~password~)`


You can change your password to something easy to remember, like: `R00k13!`

It dawned upon the powers that be that you may need to know ***how to login***
- use your favorite web browser and go here: https://(~HOSTNAME~)
- on your keyboard press and hold the `(~ctrlKeys~)` keys together in sequence then let go
- in the command console that pops up, type `login (~username~)` and hit the `Enter` key
- now you are prompted for your password, just enter your password .. copy & paste works too
- if all went well, you should no longer be anonymous .. well done ;)
- to change your password, use: `passwd`

Welcome to the team!
